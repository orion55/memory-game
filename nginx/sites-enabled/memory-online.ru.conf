server {
    listen 80;
    listen [::]:80;
    server_name memory-online.ru;
    server_tokens off;

    if ($host = memory-online.ru) {
        return 301 https://$host$request_uri;
    }
}

server {
    listen [::]:443 ssl ipv6only=on;
    listen 443 ssl;
	http2 on;

    server_name memory-online.ru;

    location / {
        proxy_pass  http://server:3000;
        include     config/proxy.conf;
    }

    ssl_certificate /etc/letsencrypt/live/memory-online.ru/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/memory-online.ru/privkey.pem;

    include config/options-ssl-nginx.conf;

    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

    add_header Strict-Transport-Security "max-age=31536000" always;

    ssl_trusted_certificate /etc/letsencrypt/live/memory-online.ru/chain.pem;
    ssl_stapling on;
    ssl_stapling_verify on;

    location /.well-known/acme-challenge {
        allow all;
        root /var/www/certbot;
    }
}