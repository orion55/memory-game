#!/bin/bash

COMPOSE="/usr/local/bin/docker-compose --ansi never"

cd /home/gitlab-runner/deploy/ || exit
$COMPOSE run certbot renew && $COMPOSE kill -s SIGHUP nginx
