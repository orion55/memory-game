import styles from './page.module.css';

const Home = () => {
  return (
    <div className={styles.main}>
      <h1 className={styles.main__title}>Memory Game</h1>
      <button className={styles.main__button} type='button'>
        Start
      </button>
    </div>
  );
};

export default Home;
