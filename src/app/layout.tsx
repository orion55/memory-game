import type { Metadata } from 'next';
import { Roboto } from 'next/font/google';
import './modern-normalize.css';
import './globals.css';

const inter = Roboto({ weight: '400', subsets: ['cyrillic'] });

export const metadata: Metadata = {
  title: 'Memory Game',
  description: 'Memory Game. This is the best memory development game',
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang='ru'>
      <body className={inter.className}>{children}</body>
    </html>
  );
}
